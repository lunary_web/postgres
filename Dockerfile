FROM postgres

VOLUME /var/lib/postgresql/data

EXPOSE 5432
